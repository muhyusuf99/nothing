# ![logo](LINE-sm.png) LINE Python

*LINE Messaging's private API*

----

# Update 10 Des 2018

# Termux

```sh
$ apt update
$ apt upgrade
$ apt install python
$ pkg install python3
$ apt install git
$ git clone https://github.com/galpt/gpt
$ cd gpt
$ python -m pip install -r requirements.txt
$ python3 gpt.py
```

## VPS

```sh
$ git clone https://github.com/galpt/gpt
$ cd gpt
$ python3 -m pip install -r requirements.txt
$ python gpt.py
```

## Credit:
gal.pt

